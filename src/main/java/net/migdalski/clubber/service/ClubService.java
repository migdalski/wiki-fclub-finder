package net.migdalski.clubber.service;

import lombok.RequiredArgsConstructor;
import net.migdalski.clubber.dao.PageDao;
import net.migdalski.clubber.dao.TemplateDao;
import net.migdalski.clubber.model.search.SearchPage;
import net.migdalski.clubber.model.template.TemplatePage;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ClubService {

    private final static int MAX_PAGES = 10;
    private static final String PROTOCOL = "https";
    private static final String HOST = "en.wikipedia.org";
    private static final String PATH_FORMAT = "/wiki/%s";

    private final PageDao pageDao;

    private final TemplateDao templateDao;

    /**
     * Return a link to searching football club
     *
     * @param query to search
     * @return url to wiki page
     */
    public List<String> getClubUrl(String query) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < MAX_PAGES; i++) {
            List<SearchPage> results = pageDao.query(query, i);
            Map<String, TemplatePage> templates = templateDao.queryFootballInfoBoxes(results);
            results.stream()
                    .filter(searchPage -> isFootballClub(searchPage, templates))
                    .findAny()
                    .ifPresent(any -> result.add(createUrl(any)));
        }
        return result;
    }

    private String createUrl(SearchPage any) {
        try {
            URI uri = new URI(
                    PROTOCOL,
                    HOST,
                    String.format(PATH_FORMAT, any.getTitle()),
                    null);
            return uri.toURL().toString();
        } catch (URISyntaxException | MalformedURLException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }

    private boolean isFootballClub(SearchPage searchPage, Map<String, TemplatePage> templates) {
        return templates.get(searchPage.getPageid()).getTemplates() != null
                && !templates.get(searchPage.getPageid()).getTemplates().isEmpty();
    }
}
