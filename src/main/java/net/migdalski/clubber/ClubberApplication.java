package net.migdalski.clubber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClubberApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClubberApplication.class, args);
    }

}
