package net.migdalski.clubber.dao;

//
import net.migdalski.clubber.model.search.SearchPage;
import net.migdalski.clubber.model.template.TemplatePage;
import net.migdalski.clubber.model.template.Templates;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class TemplateDao extends WikiDao {

    private static final String URL_FORMAT = "%s?action=query&format=json&prop=templates&titles=%s&tllimit=1&tltemplates=Template:Infobox%%20football%%20club";

    public Map<String, TemplatePage> queryFootballInfoBoxes(List<SearchPage> searchPages) {
        RestTemplate restTemplate = new RestTemplate();

        Templates templates = restTemplate.getForObject(createUrl(searchPages), Templates.class);
        if (templates ==null) {
            return Collections.emptyMap();
        }
        return templates.getQuery().getPages();
    }

    private String createUrl(List<SearchPage> searchPages) {
        return String.format(URL_FORMAT, WIKI_ENDPOINT, searchPages.stream().map(SearchPage::getTitle).collect(Collectors.joining("|")));
    }
}
