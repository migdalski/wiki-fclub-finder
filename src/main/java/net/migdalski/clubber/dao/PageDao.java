package net.migdalski.clubber.dao;

import net.migdalski.clubber.model.search.SearchPage;
import net.migdalski.clubber.model.search.Search;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

@Repository
public class PageDao extends WikiDao {

    private static final Integer LIMIT = 10;
    private static final String URL_FORMAT = "%s?action=query&list=search&format=json&srsearch=%%22%s%%22&srlimit=%d&sroffset=%d";

    public List<SearchPage> query(String query, Integer page) {
        RestTemplate restTemplate = new RestTemplate();
        Search search = restTemplate.getForObject(createUrl(query, page), Search.class);

        if (search == null) {
            return Collections.emptyList();
        }
        return (List<SearchPage>) search.getQuery().getSearch();
    }

    private String createUrl(String query, Integer page) {
        return String.format(URL_FORMAT, WIKI_ENDPOINT, query, LIMIT, LIMIT * page);
    }

}
