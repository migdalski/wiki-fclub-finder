package net.migdalski.clubber.model.template;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
public class Query {
    HashMap<String, TemplatePage> pages;
}
