package net.migdalski.clubber.model.template;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Templates {
    Query query;
}
