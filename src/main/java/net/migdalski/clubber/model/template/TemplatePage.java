package net.migdalski.clubber.model.template;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;

@Data
@NoArgsConstructor
public class TemplatePage {
    private String pageid;
    private String title;
    private Collection<Template> templates;
}