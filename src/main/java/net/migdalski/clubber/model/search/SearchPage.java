package net.migdalski.clubber.model.search;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SearchPage {
    private String pageid;
    private String title;
    private String snippet;
}
