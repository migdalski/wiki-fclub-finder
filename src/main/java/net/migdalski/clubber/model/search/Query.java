package net.migdalski.clubber.model.search;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@NoArgsConstructor
public class Query {
    Collection<SearchPage> search;
}
