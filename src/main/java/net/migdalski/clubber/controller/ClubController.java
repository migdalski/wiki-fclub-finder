package net.migdalski.clubber.controller;

import lombok.RequiredArgsConstructor;
import net.migdalski.clubber.service.ClubService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ClubController {

    private final ClubService clubService;

    @GetMapping("/club")
    public ResponseEntity<List<String>> index(@RequestParam String query) {
        return ResponseEntity.ok(clubService.getClubUrl(query));
    }
}
